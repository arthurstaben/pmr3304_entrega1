---
title: "Biografia"
slug: "biografia"
---

Olá! Meu nome é João Arthur Staben, e este é o espaço onde compartilho minha jornada de vida. Atualmente, sou estudante do 3°ano de Engenharia Mecatrônica pela Escola Politécnica da USP, nascido em Salvador-BA.

Nascido e criado em Salvador, tenho grande parte da minha história e memórias de vida na minha cidade natal. No sentido acadêmico, sempre me interessei pela área de tecnologia e inovações, atrativo este que me fez por optar pela área de engenharia e, mais especificamente, pela mecatrônica. 

Na perspectiva um pouco mais pessoal, três pilares envolvem aquilo que chamam de "hobbies". O primeiro é o futebol, muito incentivado pelo Bahia, meu clube do coração, e minha prática assídua ao longo dos anos junto de amigos e familiares. Além do esporte, a música também faz muito parte da rotina, sendo a ecleticidade a base dessa prática, toco instrumentos de corda e de percussão desde criança. Por fim, a sétima arte fecha a tríade de interesses pessoais: o cinema é uma fonte medular de lazer durante toda a minha vida.

Muito obrigado por visitar minha página de biografia!

[Home]({{< relref "_index.md" >}})[Currículo]({{< relref "curriculo.md" >}})