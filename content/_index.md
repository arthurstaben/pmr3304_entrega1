---
title: "Home"
slug: "home"
---

{{< photo-text >}}
Aqui, você será capaz de visualizar o meu portfólio pessoal, tendo a possibilidade de encontrar minha coleção de vida durante a minha existência. É possível verificar:

- A minha biografia, que inclui minha trajetória de vida e interesses pessoais. 
- Meu currículo e projetos, que permeiam diferentes atividades que constroem minha carreira profissional e acadêmica até o momento, os quais formam os pilares das minhas competências.
{{< /photo-text >}}

[Biografia]({{< relref "biografia.md" >}})[Currículo]({{< relref "curriculo.md" >}})