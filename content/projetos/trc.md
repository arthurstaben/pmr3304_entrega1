---
title: "TRC"
slug: "trc"
---

## Pesquisa de Mercado
O projet consistiu numa atuação em conjunto da Poli Júnior com a Teak Resources Company, para estudar formas de diversificar as fontes de receita da empresa por meio do reaproveitamento dos detritos florestais e resíduos industriais
que hoje são jogados fora. Assim, o intuito foi direcionar formas de estabelecer parcerias com marceneiros e
indústrias, além de entender como os concorrentes do ramo atuam. No sentido de desenvolvimento, este foi realizado a partir da rodagem de um questionário e do uso de técnicas de Analytics.