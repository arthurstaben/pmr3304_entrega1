---
title: "Grupo Datora"
slug: "datora"
---

## Gestão de Processos
Para este projeto, a empresa tinha a necessidade a melhor compreensão da área de Fullfilment, tanto da Datora quanto da nova empresa que está sendo incorporada, a fim de padronizar da melhor forma esse processo. Por isso, a ideia do projeto foi mapear e propor soluções para a área de Fullfilment, além de enxergar a questão do processo na filial, tendo como objetivos, portanto, o entendimento do cenário atual, a identificação de gargalos e a proposição de soluções.
