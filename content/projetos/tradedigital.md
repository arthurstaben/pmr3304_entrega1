---
title: "SC Trade Digital"
slug: "tradedigital"
---

## Gestão de Processos
À época, a SC Trade estava em processo de unificação de três Startups do setor tecnológico e, nesse sentido,
houve necessidade de registrar os processos realizados por cada área, bem como entender as responsabilidades de cada funcionário.
Nesse sentido, a Poli Júnior buscou por meio deste projeto mapear e unificar os processos dos setores de clientes, tecnologia e infraestrutura, identificando pontos de melhoria e otimizando as atividades executadas nessas Startups.
No fim, o projeto consistiu em toda a estruturação dos processos da empresa, bem como a sua documentação e planos de execução e distribuição de tarefas utilizando plataformas que incitavam o uso coletivo, visando uma organização contínua.