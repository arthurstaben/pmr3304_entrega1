---
title: "DUX Nutrition Lab"
slug: "dux"
---

## Gestão de Processos
Na época, a dor da empresa era que o processo de faturamento da DUX Nutrition Lab é feito de forma muito manual, o que causa uma perda da qualidade, com erros e dificuldades na comunicação, além de uma rotina pesada para o time responsável.
Tendo isso em vista, o projeto buscou mapear e formalizar os processos realizados pela área de faturamento da empresa, identificando pontos de melhoria e otimizando as atividades executadas no processo de faturamento. 