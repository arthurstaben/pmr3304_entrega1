---
title: "Petlove"
slug: "petlove"
---

## Pesquisa de Mercado
A Petlove é uma empresa que nos últimos anos vem aumentando sua participação no mercado de pets, tornando-se um dos maiores e-commerce pet do país. Assim, para manter a assertividade em seu crescimento, procurou-se entender melhor as perspectivas de seus consumidores de rações secas. A Pesquisa de Mercado irá abranger quatro categorias e os principais players de cada uma, analisando pontos de destaque,
competitividade, canais de compra, buscando um melhor direcionamento para seus novos produtos. O desenvolvimento foi feito a partir de um questionário e do uso de técnicas de Analytics. Dessa forma, o projeto foi capaz de entender as tendências e o desempenho do mercado de rações, além de uma compreensão dos clientes sobre.
