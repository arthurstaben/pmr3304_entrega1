---
title: "Currículo"
slug: "curriculo"
---

# Formação Acadêmica

#### COLÉGIO ANCHIETA (jan.2019 - dez.2020)
Curso dos dois primeiros anos do ensino médio, com gama de ensino dentre as áreas básicas de matemática, língua portuguesa e ciências naturais e humanas.
#### COLÉGIO INTEGRAL (jan.2021 - dez.2021)
Curso do último ano de ensino médio, com metodologia voltada para vestibulares, revisão e aprofundamento dos mesmos tópicos de matemática, língua portuguesa e ciências naturais e humanas.
#### UNIVERSIDADE DE SÃO PAULO - ESCOLA POLITÉCNICA (feb.2022 - )
Bacharelado em Engenharia Mecatrônica, com gama de ensino dentre as mais diversas áreas de elétrica-eletrônica, mecânica e computação, além de matérias eletivas que englobam área de comunicação, realidade virtual e relato integrado.

# Experiência Profissional

#### POLI JÚNIOR (mar.2023 - abr.2024)
##### Trainee (mar.2023 - abr.2023)
Treinamento nos portfólios de gestão de processos, pesquisa de mercado e analytics.
##### Analista de Negócios (abr.2023 - mai.2023)
Capacitação em técnica de vendas, qualificação e negociação. Mediador de 4 reuniões de negócios e venda de
um projeto.
##### Analista de Projetos (mai.2023 - abr.2024)
Execução de 5 projetos na área de consultoria, envolvendo gestão de processos, pesquisa de mercado e
planilhas financeiras. Como feedback, todos os projetos obtiveram NPS promotor e, ademais, um deles foi
indicado para o prêmio de qualidade da empresa.

[Projetos]({{< relref "projetos/_index.md" >}})

#### MONITORIA (mar.2024 - jul.2024)
Lécio e auxílio na frente da matéria de Mecânica dos Sólidos, que envolve assuntos relacionados à resistência
dos materiais.

#### HOSPITAL UNIVERSITÁRIO (ago.2024 -)
Coordenador e executor na frente do desenvolvimento de um aplicativo de formulário/prontuário eletrônico através de reconhecimento de voz.


# Habilidades

- Python | HTML | CSS | Octave | Kivy
- Pacote Office 
- Inglês (Fluente) | Francês (Básico)

# Certificações

- OLIMPÍADA BRASILEIRA DE ASTRONOMIA: Medalha de Ouro (2021)
- OLIMPÍADA BRASILEIRA DE CIÊNCIAS: Medalha de Bronze (2021)

[Home]({{< relref "_index.md" >}})[Biografia]({{< relref "biografia.md" >}})